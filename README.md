# Eigene Offline Startseite

Eine einfache, responsive Webseite, die als eigene Startseite im Browser festgelegt werden kann. 
Diese Seite bietet die Möglichkeit, Hyperlinks, Aufgaben und die Berechnung der Arbeitszeit anzuzeigen. 
Die Daten wie Aufgaben oder die Arbeitszeit von dem aktuellen Tag werden im Browser-LocalStorage gespeichert,
sodass keine Server- oder Datenbankverbindung erforderlich ist.

## Inhaltsverzeichnis

- [Installation](#installation)
- [Verwendung](#verwendung)
- [Funktionen](#funktionen)
- [Verwendete Technologien](#verwendete-technologien)
- [Anpassungen](#anpassungen)
- [Beitragen](#beitragen)
- [Vorschau](#vorschau)
- [Lizenz](#lizenz)
- [Autoren und Anerkennungen](#autoren-und-anerkennungen)

## Installation

Schritt-für-Schritt-Anleitung zur Installation des Projekts.

1. Repository klonen:

    ```bash
    git clone https://gitlab.com/MarJun1988/mj-startpage-offline.git
    ```

2. In das Projektverzeichnis wechseln:

    ```bash
    cd mj-startpage-offline
    ```

3. Öffne die `index.html` Datei in deinem bevorzugten Webbrowser.

    ```bash
    open index.html
    ```

Da das Projekt nur statische Dateien verwendet, sind keine weiteren Installationsschritte notwendig.

## Verwendung

1. Öffne die `index.html` Datei in deinem Browser.
2. Setze die Seite als deine Startseite im Browser, um sie jedes Mal beim Öffnen des Browsers automatisch anzuzeigen.
3. Füge deine bevorzugten Hyperlinks hinzu.
4. Erstelle und verwalte Aufgaben direkt auf der Seite.
5. Nutze die Funktion zur Berechnung der Arbeitszeit.

## Funktionen

- **Hyperlinks**: Speichere und verwalte deine häufig besuchten Webseiten.
- **Aufgabenverwaltung**: Füge Aufgaben hinzu, markiere sie als erledigt und lösche sie bei Bedarf.
- **Arbeitszeiterfassung**: Berechne und verfolge deine Arbeitszeit.
- **LocalStorage**: Alle Daten werden im LocalStorage des Browsers gespeichert, sodass keine externe Datenbank erforderlich ist.

## Verwendete Technologien

- **HTML**: Strukturierung der Webseite.
- **CSS**: Gestaltung und Layout der Webseite.
- **JavaScript**: Interaktive Funktionen.
- **Bootstrap**: Responsive Design und vorgefertigte Komponenten. Weitere Informationen unter [getbootstrap.com](https://getbootstrap.com).

## Anpassungen

Um die Webseite an deine Bedürfnisse anzupassen, folge diesen Schritten:

1. **js/hyperlinks.js**: Trage deine gewünschten Hyperlinks ein oder ändere sie entsprechend deiner Bedürfnisse.
2. **js/main-worktime.js**: Passe die Arbeitszeiten sowie Pausenzeiten gemäß deinen Anforderungen an.
3. **index.html**: Ändere den Titel der Seite im `<title>`-Tag entsprechend deiner Präferenz. Falls gewünscht, ersetze das Logo im `img`-Ordner durch dein eigenes Logo.

## Beitragen

Informationen darüber, wie andere zu dem Projekt beitragen können.

1. Fork das Repository
2. Erstelle einen neuen Branch (`git checkout -b feature/NeuesFeature`)
3. Commit deine Änderungen (`git commit -am 'Füge ein neues Feature hinzu'`)
4. Push den Branch (`git push origin feature/NeuesFeature`)
5. Erstelle einen neuen Pull Request

## Vorschau

Eine Vorschau der Webseite ist auf [GitLab Pages](https://mj-startpage-offline-marjun1988-8b7adeeaf7bb13d29b833249bf22325.gitlab.io/) verfügbar.

## Lizenz

Dieses Projekt steht unter der MIT Lizenz - siehe die [LICENSE](LICENSE) Datei für Details.

## Autoren und Anerkennungen

* **Marcel Junghans** - *Initiale Arbeit* - [https://gitlab.com/MarJun1988](https://gitlab.com/MarJun1988)
* **Andere Mitwirkende** - Liste der Mitwirkenden

