# Changelog

Alle nennenswerten Änderungen an diesem Projekt werden in dieser Datei dokumentiert.

## [1.0.2] - 2024-06-03

### Änderung
- Fehlerbehebung bei dem Speichern von Aufgaben
- Fehlerbehebung bei dem letzten Status der Aufgabenliste

### Hinzugefügt
- Anzeige der Versionsnummer im unteren Teil der Seite sowie Hyperlink zu den aktuellen Releases
- Tooltips bei einigne Elmenten
- Tastenkürzel für Aufgabe erstellen (Windows+Alt+a)
- Tastenkürzel für die Prirität einer Aufgabe (1,2,3)

## [1.0.1] - 2024-05-25

### Änderung
- Anpassung der Darstellung der "Headline" für Responsive Darstellung.


## [1.0.0] - 2024-05-24

### Hinzugefügt
- Initiale Veröffentlichung des Projekts.

