/**
 * Elemente im HTML Bereich
 */
const elementOfWorkTimeStart = document.getElementById(`work-time-start`);
const elementOfWorkTimeEnd = document.getElementById(`work-time-end`);

/**
 * gespeicherten Arbeitszeit aus dem Localstorage vom Browser
 */
let savedWorkTime = JSON.parse(localStorage.getItem('savedWorkTime')) || {
    workTimeStart: "",
    workTimeEnd: "",
    workTimeStartFull: "",
    workTimeEndFull: ""
};

/**
 * Setzen der Arbeitszeit in Minuten pro Tag
 *
 * Beispiel:
 *  Montag: 8Stunden 15Minuten = 495
 *  Freitag: 6Stunden 0Minuten = 360
 */
const workTimeInMinutes = {
    sunday: 0,
    monday: 495,
    tuesday: 495,
    wednesday: 495,
    thursday: 495,
    friday: 360,
    saturday: 0,
};
/**
 * Setzen der Pausenzeit in Minuten pro Tag
 *
 * Beispiel:
 *  Montag: 0Stunden 30Minuten = 30
 *  Freitag: 0Stunden 30Minuten = 30
 */
const breakTimeInMinutes = {
    sunday: 0,
    monday: 30,
    tuesday: 30,
    wednesday: 30,
    thursday: 30,
    friday: 30,
    saturday: 0,
};

/**
 *  Daten im LocalStorage aktualisieren
 */
const updateWorkTimeLocalStorage = async () => {
    await localStorage.setItem('savedWorkTime', JSON.stringify(savedWorkTime));
};

/**
 * Setzen der Uhrzeiten aus dem LocalStorage,
 * wenn die Daten vom aktuellen Tag sind
 */
const setLocalStorageDates = async () => {
    // Prüfung ob selber Tag
    if (new Date(savedWorkTime.workTimeStartFull).getDay() === new Date().getDay()) {
        elementOfWorkTimeStart.value = `${savedWorkTime.workTimeStart}`;
        elementOfWorkTimeEnd.value = `${savedWorkTime.workTimeEnd}`;
    } else {
        elementOfWorkTimeEnd.value = ``;
        elementOfWorkTimeStart.value = ``;

        savedWorkTime = {
            workTimeStart: "",
            workTimeEnd: "",
            workTimeStartFull: "",
            workTimeEndFull: ""
        };
    }
    // Ausführen der Funktion
    await updateWorkTimeLocalStorage();
};

/**
 * Berechnung des Endes der Arbeitszeit
 */
const calcWorkTime = async () => {
    // aktuellen Wochentag bestimmen
    const currentDay = new Date().getDay();
    let workTimeToCalc = 0;
    let breakTimeToCalc = 0;

    // Festlegung Wochentag, um die Arbeitsdauer und Pausendauer zu bestimmen
    switch (currentDay) {
        // Sontag
        case 0:
            workTimeToCalc = workTimeInMinutes.sunday;
            breakTimeToCalc = breakTimeInMinutes.sunday;
            break;
        // Montag
        case 1:
            workTimeToCalc = workTimeInMinutes.monday;
            breakTimeToCalc = breakTimeInMinutes.monday;
            break;
        // Dienstag
        case 2:
            workTimeToCalc = workTimeInMinutes.tuesday;
            breakTimeToCalc = breakTimeInMinutes.tuesday;
            break;
        // Mittwoch
        case 3:
            workTimeToCalc = workTimeInMinutes.wednesday;
            breakTimeToCalc = breakTimeInMinutes.wednesday;
            break;
        // Donnerstag
        case 4:
            workTimeToCalc = workTimeInMinutes.thursday;
            breakTimeToCalc = breakTimeInMinutes.thursday;
            break;
        // Freítag
        case 5:
            workTimeToCalc = workTimeInMinutes.friday;
            breakTimeToCalc = breakTimeInMinutes.friday;
            break;
        // Samstag
        case 6:
            workTimeToCalc = workTimeInMinutes.saturday;
            breakTimeToCalc = breakTimeInMinutes.saturday;
            break;
        default:
    }

    // Berechnung der gesamten Zeit auf der Arbeit (Arbeitszeit + Pause) in Millisekunden
    const totalTimeIntMilliseconds = (workTimeToCalc * 60 * 1000) + (breakTimeToCalc * 60 * 1000);

    // Teilen des Textes Arbeitsbeginn (10:00), hier der Doppelpunkt
    if (elementOfWorkTimeStart.value.includes(':')) {
        const splitWorkTime = elementOfWorkTimeStart.value.split(':');
        if (splitWorkTime.length === 2) {
            // Start Uhrzeit
            let timeStart = new Date();
            timeStart.setHours(parseInt(splitWorkTime[0]))
            timeStart.setMinutes(parseInt(splitWorkTime[1]))

            // Zwischenspeichern
            savedWorkTime.workTimeStartFull = timeStart;
            savedWorkTime.workTimeStart = elementOfWorkTimeStart.value;

            // End Uhrzeit
            let timeEnd = new Date(timeStart.getTime() + totalTimeIntMilliseconds);

            elementOfWorkTimeEnd.value = `${String(timeEnd.getHours()).padStart(2, '0')}:${String(timeEnd.getMinutes()).padStart(2, '0')}`;

            // Zwischenspeichern
            savedWorkTime.workTimeEndFulll = timeEnd;
            savedWorkTime.workTimeEnd = elementOfWorkTimeEnd.value;

            // Ausführen der Funktion
            await updateWorkTimeLocalStorage();
        }
    }
};

/**
 * Ausführen beim Verlassen des Feldes Arbeitsbeginn
 */
elementOfWorkTimeStart.addEventListener("focusout", async () => {
    // Ausführen der Funktion
    await calcWorkTime();
});
/**
 * Ausführen beim Drücken der ENTER Taste beim Feld Arbeitsbeginn
 */
elementOfWorkTimeStart.addEventListener("keypress", async (e) => {
    if (e.key === "Enter") {
        e.preventDefault();
        // Ausführen der Funktion
        await calcWorkTime();
    }
});

/**
 * Wenn die Seite komplett geladen wurde
 */
document.addEventListener("DOMContentLoaded", async () => {
    // Ausführen der Funktion
    await setLocalStorageDates();
    // Focus auf Input Arbeitebeginn
    elementOfWorkTimeStart.focus();
});