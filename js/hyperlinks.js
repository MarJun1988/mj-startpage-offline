// Hyperlinks mit URL sowie Anzeigetext und die Möglichkeit der Sortierung
const hyperlinks = [
    [
        {
            sorting: 0,
            mainHeadline: "Example Headline One"
        },
        {
            sorting: 0,
            headline: "Group One",
            children: [
                {
                    sorting: 0,
                    name: "Example One",
                    hyperlink: "https://www.Example.de"
                },
                {
                    sorting: 5,
                    name: "Example Two",
                    hyperlink: "https://www.Example.de"
                },
            ]
        },
        {
            sorting: 10,
            headline: "Group One",
            children: [
                {
                    sorting: 0,
                    name: "Example One",
                    hyperlink: "https://www.Example.de"
                },
                {
                    sorting: 5,
                    name: "Example Two",
                    hyperlink: "https://www.Example.de"
                },
            ]
        }
    ],
    [
        {
            sorting: 100,
            mainHeadline: "Example Headline Two"
        },
        {
            sorting: 110,
            headline: "Group One",
            children: [
                {
                    sorting: 0,
                    name: "Example One",
                    hyperlink: "https://www.Example.de"
                },
                {
                    sorting: 5,
                    name: "Example Two",
                    hyperlink: "https://www.Example.de"
                },
            ]
        },
        {
            sorting: 120,
            headline: "Group Two",
            children: [
                {
                    sorting: 0,
                    name: "Example One",
                    hyperlink: "https://www.Example.de"
                },
                {
                    sorting: 5,
                    name: "Example Two",
                    hyperlink: "https://www.Example.de"
                },
            ]
        }
    ]
];