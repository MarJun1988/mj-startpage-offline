/**
 * Elemente im HTML Bereich
 */
const elementOfButtons = document.querySelectorAll(`.btn`)
const elementOfToggleSiteTheme = document.getElementById(`toggle-bs-theme`);

/**
 * gespeicherten Theme aus dem Localstorage vom Browser
 */
let savedUserTheme = localStorage.getItem('savedUserTheme');

/**
 *  Daten im LocalStorage aktualisieren
 */
const updateUserThemeLocalStorage = () => {
    localStorage.setItem('savedUserTheme', savedUserTheme);
};

/**
 * Die Seite auf das jeweilige Aussehen des Nutzers setzen
 * @param value "light" or "dark"
 */
const setUserTheme = async (value) => {
    if (value) {
        document.documentElement.setAttribute('data-bs-theme', value);
        savedUserTheme = value;
        await updateUserThemeLocalStorage();
    }
};
/**
 * Anpassen der Knöpfe auf das jeweilige Theme
 * @param value "light" or "dark"
 */
const changeButtonColors = (value) => {
    // alle Knöpfe Durchlaufen
    if (elementOfButtons && elementOfButtons.length > 0) {
        for (let i = 0; i < elementOfButtons.length; i++) {
            if (value === "dark") {
                elementOfButtons[i].classList.remove('btn-outline-dark')
                elementOfButtons[i].classList.add('btn-outline-light')
            } else {
                elementOfButtons[i].classList.remove('btn-outline-light')
                elementOfButtons[i].classList.add('btn-outline-dark')
            }
        }
    }
};

/**
 * Legt das standardmäßige Thema für den Benutzer fest.
 *
 * Diese Funktion bestimmt das standardmäßige Thema für den Nutzer basierend auf dem gespeicherten Benutzerthema und dem Browserthema.
 * Wenn das Benutzerthema nicht gespeichert ist, verwendet es das Browserthema als Standardthema.
 * Danach stellt es das Benutzerthema als neues Standardthema ein,
 * speichert das Benutzerthema in einem persistenten Speicher,
 * und ändert die Farbe der Schaltflächen, um sie an das neue Thema anzupassen.
 * @returns {void}
 */
const setDefaultTheme =async () => {
    const newTheme = savedUserTheme === null ? getBrowserTheme() : savedUserTheme;
    await setUserTheme(newTheme)
    await changeButtonColors(newTheme);
}

/**
 * Ruft das aktuelle Browser-Thema ab.
 *
 * @returns {string} Das aktuelle Browser-Thema, entweder 'dark' oder 'light'.
 */
const getBrowserTheme = () => {
    return window &&
    window.matchMedia &&
    window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
};

/**
 * Bei Klick des Knopfes "Umschaltung Theme" reagieren
 *
 */
elementOfToggleSiteTheme.addEventListener('click', async () => {
    const newTheme = savedUserTheme === 'dark' ? 'light' : 'dark';
     // Ausführen der Funktion
    await setUserTheme(newTheme)
    await changeButtonColors(newTheme);
});

/**
 * Wenn die Seite komplett geladen wurde
 */
document.addEventListener("DOMContentLoaded", async () => {
    // Ausführen der Funktion
    await setDefaultTheme();

    // Tooltip aktivieren
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
});