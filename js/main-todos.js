// Elemente im HTML Bereich
const elementOfTodoList = document.getElementsByClassName(`todo-list`);
const elementOfTodoEdit = `edit-todo-btn`;
const elementOfTodoDelete = `delete-todo-btn`;
const elementOfTodoComplete = `complete-todo-btn`;
const elementOfTodoVisibleCompleted = document.getElementsByClassName(`toggle-completed-todos-btn`);
const elementOfExportTodos = document.getElementsByClassName(`export-todos`);
const elementOfImportTodos = document.getElementsByClassName(`import-todos`);
const elementOfImportTodosInput = document.getElementsByClassName(`file-input`);
const elementOfButtonNewTodo = document.getElementsByClassName(`new-todo`);
const elementOfTodoTitle = document.getElementById(`input-title`);
const elementOfTodoNotes = document.getElementById(`input-notes`);
const elementOfTodoPriority = document.getElementById(`select-priority`);
const elementOfTodoIndex = document.getElementById(`input-id`);
const elementOfTodoActionButtonModal = document.getElementById(`modal-action-btn`);
const elementOfTodoModal = document.getElementById(`modal-todo`);
const elementOfTodoAsSwitch = document.getElementById(`switch-show-todos`);
const elementOfTodoAsOffcanvas = document.getElementById(`todo-offcanvas`);
const elementOfTodoAsFlex = document.getElementById(`todo-flex`);
const elementOfButtonShowTodoList = document.getElementById(`show-todo-offcanvas-btn`);


// gespeicherten Aufgabe aus dem Localstorage vom Browser
let savedTodoList = JSON.parse(localStorage.getItem('savedTodoList')) || [];
// Standard Anzeige der Aufgaben-Liste, aus dem Localstorage vom Browser
let showedTodoList = localStorage.getItem('showedTodoList') || false;
// Anzeige der Aufgaben-Liste als Offcanvas oder direkt im Anzeigebereich (Standard, im Anzeige Bereich)
let showTodoListAsOffcanvas = localStorage.getItem('showTodoListAsOffcanvas') || elementOfTodoAsOffcanvas.checked;

// Initialisierung des Offcanvas
const bsOffcanvas = new bootstrap.Offcanvas('#todo-offcanvas');

/**
 * Aufgaben aus dem Localstorage lade und im Aufgabenbereich ausgeben
 *
 * @returns {Promise<void>}
 */
const generateTodosFromLocalStorage = async () => {
    for (let i = 0; i < elementOfTodoList.length; i++) {
        elementOfTodoList[i].innerHTML = '';

        if (savedTodoList && savedTodoList.length > 0) {
            // Sortierung der Priorität
            const sortTodoList = await savedTodoList.sort((a, b) => {
                let aNew = !isNaN(a.priority) ? parseInt(a.priority) : 100
                let bNew = !isNaN(b.priority) ? parseInt(b.priority) : 100
                return aNew - bNew;
            });
            // Durchlaufen der gespeicherten Aufgaben
            await sortTodoList.forEach((todo, index) => {
                const listItem = document.createElement('li');
                listItem.className = "list-group-item d-flex justify-content-between align-items-start align-items-center";

                let completeDate;
                let completeDateHtml = "";

                if (todo && todo.completed) {
                    listItem.classList.add("completed")
                    listItem.classList.add("d-none")

                    completeDate = new Date(todo.completedDate);

                    completeDate = `${completeDate.toLocaleString()} `

                    completeDateHtml = `<br><br><span class="text-success">abgeschlossen am: <br> ${completeDate}</span>`
                }
                let classPill;
                // Farbe für die Priorität
                switch (parseInt(todo.priority)) {
                    case 10:
                        classPill = "text-bg-danger";
                        break;
                    case 20:
                        classPill = "text-bg-warning";
                        break;
                    case 30:
                        classPill = "text-bg-info";
                        break;
                    default:
                        classPill = "text-bg-primary";
                }

                // Zusammensetzen des li Elements
                listItem.innerHTML = `
                        <div class="ms-2">
                            <input class="form-check-input border-secondary pl-2 complete-todo-btn" type="checkbox" value="" ${todo.completed ? 'checked' : ''} id="check-complete-${index}" data-index="${index}">
                        </div>
                        <div class="ms-2 me-auto text-start">
                          <div class="fw-bold">${todo.title}</div>                 
                            <span>${todo.notes}</span>                
                            ${completeDateHtml}                
                        </div>
                        <div class="text-end">
                            <span class="badge ${classPill} rounded-pill mb-1">Priorität: ${todo.priority}</span>
                            <br>
                            <a href="#"><i class="bi bi-pencil edit-todo-btn pe-auto" data-index="${index}" data-bs-toggle="modal" data-bs-target="#modal-todo"></i></a>
                            <a href="#"><i class="bi bi-trash delete-todo-btn ms-1 pe-auto" data-index="${index}"></i></a>
                        </div>`;

                elementOfTodoList[i].appendChild(listItem);
            });
        }
    }
};

/**
 *  Daten im LocalStorage aktualisieren
 */
const updateTodoLocalStorage = async () => {
    await localStorage.setItem('savedTodoList', JSON.stringify(savedTodoList));
    await localStorage.setItem('showedTodoList', showedTodoList);
    await localStorage.setItem('showTodoListAsOffcanvas', showTodoListAsOffcanvas);
};

/**
 * Neue Aufgabe erstellen bzw. Speichern
 *
 * @returns {Promise<void>}
 */
const newTodo = async () => {
    // hinzufügen des Eintrages
    savedTodoList.push({
        title: elementOfTodoTitle.value,
        notes: elementOfTodoNotes.value,
        priority: elementOfTodoPriority.value,
        completed: false,
        completedDate: "",
        createdAt: new Date(),
        updateAt: new Date(),
    });

    // Ausführen der Funktionen
    await updateTodoLocalStorage();
    await clearTodoModal();
    await generateTodosFromLocalStorage();
};

/**
 * Aufgabe bearbeite bzw. aktualisieren
 *
 * @returns {Promise<void>}
 */
const editTodo = async () => {
    if (elementOfTodoIndex.value && savedTodoList[elementOfTodoIndex.value]) {
        // aktualisieren des Eintrages
        savedTodoList[elementOfTodoIndex.value].title = elementOfTodoTitle.value;
        savedTodoList[elementOfTodoIndex.value].notes = elementOfTodoNotes.value;
        savedTodoList[elementOfTodoIndex.value].priority = elementOfTodoPriority.value;
        savedTodoList[elementOfTodoIndex.value].updateAt = new Date();
    } else {
        console.log(`Es wurde keine ID in dieser Aufgabe gefunden!`)
        // Ausführen der Funktionen
        await newTodo();
    }
    // Ausführen der Funktionen
    await updateTodoLocalStorage();
    await clearTodoModal();
    await generateTodosFromLocalStorage();
};

/**
 * Aufgabe als erledigt markieren
 *
 * @returns {Promise<void>}
 */
const completedTodo = async () => {
    if (elementOfTodoIndex.value) {
        // aktualisieren des Eintrages
        savedTodoList[elementOfTodoIndex.value].completed = true;
        savedTodoList[elementOfTodoIndex.value].completedDate = new Date()
    } else {
        console.log(`Es wurde keine ID in dieser Aufgabe gefunden!`)
    }
    // Ausführen der Funktion
    await updateTodoLocalStorage();
    await generateTodosFromLocalStorage();
};

/**
 * Leeren der Eingabefelder, vom Aufgaben Dialog
 *
 * @returns {Promise<void>}
 */
const clearTodoModal = async () => {
    elementOfTodoTitle.value = "";
    elementOfTodoNotes.value = "";
    elementOfTodoPriority.value = "100";
    elementOfTodoIndex.value = "";
};

/**
 * Anzeige der Aufgaben als Offcanvas oder Flex (im Anzeigebereich)
 *
 * @returns {Promise<void>}
 */
const showTodoAsOffcanvasOrFlex = async () => {
    // Prüfen, ob als Slide oder Offcanvas Anzeige
    if (showTodoListAsOffcanvas === "true") {
        elementOfTodoAsSwitch.checked = true;
    }
    // Sollen die Aufgaben angezeigt werden?
    if (showedTodoList === "true") {
        if (showTodoListAsOffcanvas === "true") {
            await bsOffcanvas.show('');
        } else {
            elementOfTodoAsFlex.classList.toggle('show');
            elementOfTodoAsFlex.classList.toggle('h-100');
            elementOfTodoAsFlex.getElementsByClassName('todo-list')[0].classList.toggle('show');
        }
    }
};

/**
 * Export der vorhanden Aufgaben als JSON Datei
 *
 * @returns {Promise<void>}
 */
const exportTodoAsJson = async () => {
    const blob = new Blob([JSON.stringify(savedTodoList, null, 2)], {
        type: 'application/json',
    });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = `todos_from_${new Date().toISOString()}.json`;
    a.click();
    URL.revokeObjectURL(url);
};

/**
 * Import der Aufgaben als JSON Datei vom Dateisystem
 *
 * @returns {Promise<void>}
 */
const importTodoAsJson = async () => {
    const result = confirm("Achtung! Es werden alle Vorhanden Überschrieben!");
    if (result) {
        await elementOfImportTodosInput[0].click();

        elementOfImportTodosInput[0].addEventListener('change', async (event) => {
            const file = event.target.files.item(0)
            const text = await file.text();
            savedTodoList = JSON.parse(text);
            await updateTodoLocalStorage();
            await generateTodosFromLocalStorage();
        });
    }
};

/**
 * Wenn die Seite komplett geladen wurde
 */
document.addEventListener("DOMContentLoaded", async () => {
    // Ausführen der Funktion
    await generateTodosFromLocalStorage();
    await showTodoAsOffcanvasOrFlex();

    // Darstellungsart der Aufgaben
    elementOfTodoAsSwitch.addEventListener("click", async () => {
        showTodoListAsOffcanvas = elementOfTodoAsSwitch.checked;
        // Ausführen der Funktion
        await updateTodoLocalStorage();
    });

    // Reagieren beim Druck auf "ENTER"
    elementOfTodoModal.addEventListener("keypress", async (e) => {
        if (e.key === "Enter") {
            e.preventDefault();
            // Ausführen eines Klicks auf erstellen bzw. speichern
            await elementOfTodoActionButtonModal.click();
        }
    });

    // Auf die Tasten 1,2,3 reagieren beim Modal (neue Aufgabe)
    elementOfTodoModal.addEventListener("keydown", async (e) => {
        switch (e.key) {
            case "1":
                elementOfTodoPriority.value = 10;
                break;
            case "2":
                elementOfTodoPriority.value = 20;
                break;
            case "3":
                elementOfTodoPriority.value = 30;
                break;
        }
    });

    // Auf die Tasten Alt+a reagieren (neue Aufgabe)
    document.body.addEventListener("keydown", async (e) => {
        if (e.altKey && e.metaKey && e.key === "a") {
            await elementOfButtonNewTodo[1].click();
        }
    });

    // Reagieren beim Klick auf den Knopf "erstellen" bzw. "speichern" im Modal
    elementOfTodoActionButtonModal.addEventListener('click', async () => {
        if (elementOfTodoIndex.value) {
            await editTodo()
        } else {
            await newTodo();
        }
    });

    // Soll die Aufgaben-Liste beim Laden angezeigt werden
    elementOfButtonShowTodoList.addEventListener("click", async () => {
        if (elementOfTodoAsSwitch.checked) {
            if (elementOfTodoAsOffcanvas.classList.contains('show')) {
                bsOffcanvas.hide();
                // Sichtbarkeit beim Aufrufen
                showedTodoList = false;
            } else {
                bsOffcanvas.show('');
                // Sichtbarkeit beim Aufrufen
                showedTodoList = true;
            }
        } else {
            elementOfTodoAsFlex.classList.toggle('show');
            elementOfTodoAsFlex.classList.toggle('h-100');
            elementOfTodoAsFlex.getElementsByClassName('todo-list')[0].classList.toggle('show');

            // Sichtbarkeit beim Aufrufen
            showedTodoList = elementOfTodoAsFlex.classList.contains('show');
        }
        // Slider oder Offcanvas
        showTodoListAsOffcanvas = elementOfTodoAsSwitch.checked;
        // Ausführen der Funktion
        await updateTodoLocalStorage();
    });

    // Wenn Modal offen, Focus auf Titel
    elementOfTodoModal.addEventListener('shown.bs.modal', async () => {
        await elementOfTodoTitle.focus();
    });

    // Anhängen des Event Listeners an der Class "TodoList"
    for (const todoList of elementOfTodoList) {
        todoList.addEventListener('click', async (e) => {

            // Knopf löschen (Mülltone)
            if (e.target.classList.contains(elementOfTodoDelete)) {
                savedTodoList.splice(parseInt(e.target.dataset.index), 1);
                // Knopf bearbeiten (Stift)
            } else if (e.target.classList.contains(elementOfTodoEdit)) {
                // Bezeichnung des Knopfes im Modal
                elementOfTodoActionButtonModal.textContent = "speichern";
                // Index
                elementOfTodoIndex.value = parseInt(e.target.dataset.index);
                // Inputfelder befüllen
                elementOfTodoTitle.value = savedTodoList[parseInt(e.target.dataset.index)].title
                elementOfTodoNotes.value = savedTodoList[parseInt(e.target.dataset.index)].notes
                elementOfTodoPriority.value = savedTodoList[parseInt(e.target.dataset.index)].priority
                // Checkbox (Eintrag als erledigt markieren)
            } else if (e.target.classList.contains(elementOfTodoComplete)) {
                // Index
                elementOfTodoIndex.value = e.target.dataset.index;
                // Ausführen der Funktion
                await completedTodo();
            }

            // Ausführen der Funktionen
            await updateTodoLocalStorage();
            await generateTodosFromLocalStorage();
        })
    }

    // Ein-/Ausblenden von erledigten Aufgaben
    for (const visibleCompleted of elementOfTodoVisibleCompleted) {
        visibleCompleted.addEventListener("click", async () => {
            // Umschalten des Icons
            visibleCompleted.children[0].classList.toggle('bi-eye-slash');
            visibleCompleted.children[0].classList.toggle('bi-eye');

            // Ausblenden der erledigten Aufgaben
            document.querySelectorAll('.list-group-item.completed').forEach(todo => {
                todo.classList.toggle("d-none");
            });
        })
    }

    // Export der Aufgaben
    for (const elementExport of elementOfExportTodos) {
        elementExport.addEventListener("click", async (e) => {
            await e.preventDefault()
            await exportTodoAsJson();
        })
    }

    // Import der Aufgaben
    for (const elementImport of elementOfImportTodos) {
        elementImport.addEventListener("click", async (e) => {
            await e.preventDefault()
            await importTodoAsJson();
        })
    }
});