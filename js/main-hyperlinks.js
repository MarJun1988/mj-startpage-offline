// Elemente im HTML Bereich
const elementOfHyperlinkMainRow = document.getElementById(`main-row`);

/**
 * Dynamische befüllung der Seite mit den Hyperlinks
 */
const generateHyperlinksFromFileSystem = async () => {
    if (hyperlinks && hyperlinks.length > 0) {
        // Sortierung der Abschnitte
        const sortHyperlinks = hyperlinks.sort(function (a, b) {
            return a.sorting - b.sorting;
        });

        // Durchlaufen aller Einträge
        sortHyperlinks.forEach((hyperlinksGroups, index) => {
            if (hyperlinksGroups && hyperlinksGroups.length > 0) {
                const sortingGroups = hyperlinksGroups.sort(function (a, b) {
                    return a.sorting - b.sorting;
                });

                // Die einzelnen Abschnitte durchlaufen
                sortingGroups.forEach(groupElement => {
                    // Erstelle ein neues div-Element
                    let newDiv = document.createElement('div');
                    // Prüfen, ob dies "Nur" die Überschrift ist
                    if (groupElement && groupElement.mainHeadline) {
                        // Class dem div zuordnen
                        newDiv.classList.add("col-12");
                        newDiv.classList.add("mb-2");
                        newDiv.classList.add("text-danger");

                        // Wenn erste Überschrift, dann oben einen Abstand hinzufügen
                        if (index >= 1) {
                            newDiv.classList.add("mt-3");
                        }

                        // Erstelle H3 Überschrift
                        let newHeadline = document.createElement('h3');
                        newHeadline.innerHTML = `<strong>${groupElement.mainHeadline}</strong>`

                        // Füge die Headline zum div-Element hinzu
                        newDiv.appendChild(newHeadline);
                        // Füge das neue div zum Zielfeld hinzu
                        elementOfHyperlinkMainRow.appendChild(newDiv)
                    } else if (groupElement && groupElement.headline) {
                        // Class dem div zuordnen
                        newDiv.classList.add("col");

                        // Erstelle H5 Überschrift
                        let newHeadline = document.createElement('h5');
                        newHeadline.innerHTML = `<strong>${groupElement.headline}</strong>`

                        // Erstelle ein neues ul-Element
                        let newUlList = document.createElement('ul');
                        newUlList.classList.add("list-group");
                        newUlList.classList.add("list-group-flush");

                        // Sortierung der einzelnen Hyperlinks
                        const sortLinks = groupElement.children.sort(function (a, b) {
                            return a.sorting - b.sorting;
                        });

                        // erstellen der einzelnen li Element
                        sortLinks.forEach(link => {
                            // li Element
                            let newLiElement = document.createElement('li');
                            newLiElement.classList.add("list-group-item");
                            // a Element
                            let newAElement = document.createElement('a');
                            newAElement.target = "_blank";
                            newAElement.href = `${link.hyperlink}`
                            newAElement.text = `${link.name}`

                            newLiElement.appendChild(newAElement);
                            newUlList.appendChild(newLiElement);
                        })

                        // Füge die ul-Liste zum div-Element hinzu
                        newDiv.appendChild(newHeadline);
                        newDiv.appendChild(newUlList);

                        // Füge das neue div zum Zielfeld hinzu
                        elementOfHyperlinkMainRow.appendChild(newDiv)
                    }
                })
            }
        })
    }
};

/**
 * Wenn die Seite komplett geladen wurde
 */
document.addEventListener("DOMContentLoaded", async () => {
    // Ausführen der Funktion
    await generateHyperlinksFromFileSystem();
});